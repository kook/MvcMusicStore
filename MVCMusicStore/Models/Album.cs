﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMusicStore.Models
{
    //表示在进行数据绑定的时候，不包括 AlbumId 这个属性。
    [Bind(Exclude="AlbumId")]
    public class Album
    {
        [ScaffoldColumn(false)]
        public int AlbumId { get; set; }
        [DisplayName("Genre")]
        public int GenreId { get; set; }
        [DisplayName("Artist")]
        public int ArtistId { get; set; }

        [Required(ErrorMessage = "不能为空")]
        [StringLength(160)]
        public string Title { get; set; }

        [Required(ErrorMessage = "不能为空")]
        [Range(0.01,100.00,ErrorMessage = "必须在0-100之内")]
        public decimal Price { get; set; }

        [DisplayName("Album Art Url")]
        [StringLength(1024)]
        public string AlbumArtUrl { get; set; }

        public virtual  Genre Genre { get; set; }
        public virtual Artist Artist { get; set; }
        public virtual List<OrderDetail> OrderDetails { get; set; }
    }
}